class AddIndexesForCreatedAt < ActiveRecord::Migration[5.2]
  def change
    add_index :categories, :created_at
    add_index :products, :created_at
  end
end
