# == Schema Information
#
# Table name: categories
#
#  id             :uuid             not null, primary key
#  parent_id      :uuid
#  name           :string
#  products_count :integer
#  created_at     :datetime         not null
#  updated_at     :datetime         not null
#  g_identifier   :string
#

class Category < ApplicationRecord
  include Swagger::Blocks

  default_scope -> { order("created_at ASC") }

  swagger_schema :CategoryJSON do
    key :required, [:id, :name]

    property :id do
      key :type, :uuid
    end

    property :name do
      key :type, :string
    end

    property :displayName do
      key :type, :string
      key :description, 'virtual field, name for displaying a record in the UI'
    end

    property :parentId do
      key :type, :uuid
      key :description, 'self reference to ID of parent category'
    end

    property :parentName do
      key :type, :string
      key :description, 'virtual field, references name of parent category'
    end

    property :productsCount do
      key :type, :integer
      key :description, 'counter cache for product records'
    end

    property :uniqueIdentifier do
      key :type, :string
      key :description, 'unique identifier in the format CC-CC-CC'
    end
  end

  swagger_schema :CategoryOption do
    property :value do
      key :type, :integer
      key :description, 'references ID field'
    end

    property :text do
      key :type, :string
      key :description, 'references name field'
    end
  end

  swagger_schema :CategoryCount do
    property :count do
      key :type, :integer
    end
  end

  swagger_schema :CategoryCreate do
    property :name do
      key :type, :string
    end

    property :parentId do
      key :type, :uuid
    end
  end

  swagger_schema :CategoryUpdate do
    property :id do
      key :type, :uuid
    end

    property :name do
      key :type, :string
    end

    property :parentId do
      key :type, :uuid
    end
  end

  # scope for searching by name
  scope :with_name, ->(query) { where("name LIKE ?", "%#{sanitize_sql_like(query)}%") }

  # format for g_identifier: CC-CC-CC
  has_unique_identifier :g_identifier, segment_count: 3, segment_size: 2, delimiter: '-'

  validates :name, presence: true

  belongs_to :parent, class_name: 'Category', required: false
  has_many :products, dependent: :destroy

  delegate :name, to: :parent, prefix: true, allow_nil: true

  alias_attribute :text, :name
  alias_attribute :value, :id

  alias_attribute :parentId, :parent_id
  alias_attribute :parentName, :parent_name

  alias_attribute :productsCount, :products_count

  alias_attribute :uniqueIdentifier, :g_identifier

  def displayName
    parent.present? ? "#{parent.displayName} > #{name}" : name
  end

  def as_json(options = {})
    super(
      options.keys.any? ? options : {
        only: [:id, :name],
        methods: [:displayName, :parentId, :parentName, :productsCount, :uniqueIdentifier]
      }
    )
  end

  # @return ApplicationRecord Relation: it fetch Categories by name with pagination
  # it returns all the Categories when name is empty.
  def self.with_name_and_pagination(name: '', limit: nil, offset: 0)
    with_name(name).limit(limit).offset(offset)
  end
end
