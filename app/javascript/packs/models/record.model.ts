interface Record {
  id: number;
  displayName: string;
  uniqueIdentifier: string;
}

export default Record;
