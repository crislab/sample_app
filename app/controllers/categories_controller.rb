class CategoriesController < ApplicationController
  include Swagger::Blocks

  # GET /categories/count

  swagger_path '/categories/count' do
    operation :get do
      key :summary, 'Total number of all categories'
      key :description, 'Returns count for all categories'
      key :operationId, 'countCategories'

      key :tags, [
        'category'
      ]

      response 200 do
        key :description, 'counter response'

        schema do
          key :'$ref', :CategoryCount
        end
      end

      response :default do
        key :description, 'unknown error'
      end
    end
  end

  def count
    render json: { count: Category.count }
  rescue StandardError => e
    logger.fatal "ERROR: when counting Categories: #{e}"
    render json: {notification: {level: 'error', message: 'Cannot count categories: unknown error!'}}, status: :internal_server_error
  end

  # GET /categories

  swagger_path '/categories' do
    operation :get do
      key :summary, 'Fetch all categories'
      key :description, 'Returns all categories, includes support for pagination and basic fulltext search'
      key :operationId, 'findCategories'

      key :tags, [
        'category'
      ]

      parameter do
        key :name, :query
        key :in, :query
        key :description, 'search term to filter results by'
        key :required, false
        key :type, :string
      end

      parameter do
        key :name, :per_page
        key :in, :query
        key :description, 'maximum number of results to return, used for pagination'
        key :required, false
        key :type, :integer
      end

      parameter do
        key :name, :page
        key :in, :query
        key :description, 'selected results page, used for pagination'
        key :required, false
        key :type, :integer
      end

      response 200 do
        key :description, 'category response'

        schema do
          key :type, :array

          items do
            key :'$ref', :CategoryJSON
          end
        end
      end

      response :default do
        key :description, 'unknown error'
      end
    end
  end

  def index
    limit, offset = limit_and_offset
    categories = Category.with_name_and_pagination(name: query_param, limit: limit, offset: offset)

    render json: categories.as_json
  rescue StandardError => e
    logger.fatal "ERROR: Category#index: #{e}"
    render json: {notification: {level: 'error', message: 'Cannot list categories: unknown error!'}}, status: :internal_server_error
  end

  # GET /categories/:id

  swagger_path '/categories/{id}' do
    operation :get do
      key :summary, 'Find category by ID'
      key :description, 'Returns a single category'
      key :operationId, 'findCategoryById'

      key :tags, [
        'category'
      ]

      parameter do
        key :name, :id
        key :in, :path
        key :description, 'ID of category to fetch'
        key :required, true
        key :type, :integer
      end

      response 200 do
        key :description, 'category response'

        schema do
          key :'$ref', :CategoryJSON
        end
      end

      response :not_found do
        key :description, 'no record found for submitted ID'
      end

      response :default do
        key :description, 'unknown error'
      end
    end
  end

  def show
    category = Category.find(params[:id])
    render json: category.as_json
  rescue ActiveRecord::RecordNotFound
    logger.fatal "ERROR: Category#show not found id #{params[:id]}"
    render json: {notification: {level: 'error', message: 'Cannot show category: record not found!'}}, status: :not_found
  rescue StandardError => e
    logger.fatal "ERROR: Category#show: #{e}"
    render json: {notification: {level: 'error', message: 'Cannot show category: unknown error!'}}, status: :internal_server_error
  end

  # DELETE /categories/:id

  swagger_path '/categories/{id}' do
    operation :delete do
      key :summary, 'Delete category by ID'
      key :description, 'Removes a single category from database'
      key :operationId, 'removeCategoryById'

      key :tags, [
        'category'
      ]

      parameter do
        key :name, :id
        key :in, :path
        key :description, 'ID of category to delete'
        key :required, true
        key :type, :integer
      end

      response 200 do
        key :description, 'category response (returns the record that has been destroyed)'

        schema do
          key :'$ref', :CategoryJSON
        end
      end

      response :not_found do
        key :description, 'no record found for submitted ID'
      end

      response :default do
        key :description, 'unknown error'
      end
    end
  end

  def destroy
    category = Category.find(params[:id]).destroy
    render json: category.as_json
  rescue ActiveRecord::RecordNotFound
    logger.fatal "ERROR: Category#destroy not found id #{params[:id]}"
    render json: {notification: {level: 'error', message: 'Cannot destroy category: record not found!'}}, status: :not_found
  rescue StandardError => e
    logger.fatal "ERROR: Category#show: #{e}"
    render json: {notification: {level: 'error', message: 'Cannot destroy category: unknown error!'}}, status: :internal_server_error
  end

  # PUT /categories/:id

  swagger_path '/categories/{id}' do
    operation :put do
      key :summary, 'Modify existing category'
      key :description, 'Updates an existing category and stores the record in the database after model validation'
      key :operationId, 'updateCategory'

      key :tags, [
        'category'
      ]

      parameter do
        key :name, :category
        key :in, :body
        key :description, 'Category data to update'
        key :required, true

        schema do
          key :'$ref', :CategoryUpdate
        end
      end

      response 200 do
        key :description, 'category response'

        schema do
          key :'$ref', :CategoryJSON
        end
      end

      response :not_found do
        key :description, 'no record found for submitted ID'
      end

      response :default do
        key :description, 'unknown error'
      end
    end
  end

  def update
    category = Category.find(params[:id])

    if category.update_attributes(company_params)
      render json: category.as_json
    else
      render json: {notification: {level: 'error', message: 'Cannot update category!'}}, status: :not_acceptable
    end
  rescue ActiveRecord::RecordNotFound
    logger.fatal "ERROR Category#update RecordNotFound #{params[:id]}"
    render json: {notification: {level: 'error', message: 'Cannot update category: record not found!'}}, status: :not_found
  rescue StandardError => e
    logger.fatal "ERROR: Category#update: #{e}"
    render json: {notification: {level: 'error', message: 'Cannot update category: unknown error!'}}, status: :internal_server_error
  end

  # POST /categories

  swagger_path '/categories' do
    operation :post do
      key :summary, 'Add new category'
      key :description, 'Creates a new category and stores the record in the database after model validation'
      key :operationId, 'addCategory'

      key :tags, [
        'category'
      ]

      parameter do
        key :name, :category
        key :in, :body
        key :description, 'Category to add'
        key :required, true

        schema do
          key :'$ref', :CategoryCreate
        end
      end

      response 200 do
        key :description, 'category response'

        schema do
          key :'$ref', :CategoryJSON
        end
      end

      response :not_acceptable do
        key :description, 'model validation failed'
      end

      response :default do
        key :description, 'unknown error'
      end
    end
  end

  def create
    category = Category.new(company_params)

    if category.save
      render json: category.as_json
    else
      render json: {notification: {level: 'error', message: 'Cannot create category!'}}, status: :not_acceptable
    end
  rescue StandardError => e
    logger.fatal "ERROR: Category#create: #{e}"
    render json: {notification: {level: 'error', message: 'Cannot create category: unknown error!'}}, status: :internal_server_error
  end

  # GET /categories/options

  swagger_path '/categories/options' do
    operation :get do
      key :summary, 'Fetch all categories'
      key :description, 'Returns list of all categories for use with HTML select form field'
      key :operationId, 'categoriesAsSelectOptions'

      key :tags, [
        'category'
      ]

      response 200 do
        key :description, 'category response'

        schema do
          key :type, :array

          items do
            key :'$ref', :CategoryOption
          end
        end
      end

      response :default do
        key :description, 'unknown error'
      end
    end
  end

  def options
    categories = Category.order(:name)
    render json: categories.as_json(only: [], methods: [:value, :text])
  rescue StandardError => e
    logger.fatal "ERROR: Category#options: #{e}"
    render json: {notification: {level: 'error', message: 'Cannot fetch categories: unknown error!'}}, status: :internal_server_error
  end

  private

  def company_params
    params.require(:category).permit(:name, :parentId)
  end

  # @return String: either the query for search by name, or empty for everything
  def query_param
    params[:query].presence || ''
  end

  # @return Array: [limit, offset]
  def limit_and_offset
    return [nil, 0] if params[:per_page].blank? && params[:page].blank?

    limit = params[:per_page].to_i
    page = params[:page].to_i

    offset = limit * (page - 1)

    [limit, offset]
  end
end
